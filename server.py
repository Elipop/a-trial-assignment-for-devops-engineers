#!/usr/bin/env python3
# pylint: disable=C0103, W0107, C0115, C0116

"""
    Basic HTTP Server with no fancy features.

    Use environment variables to change listen configuration:
    export LISTEN_ADDRESS="127.0.0.1"
    export LISTEN_PORT="9000"
"""

from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
import os


class HTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):

        message = """El perro es mi corazón""".encode()

        self.send_response(200)
        self.send_header("Content-Type", "text/plain;charset=UTF-8")
        self.end_headers()
        self.wfile.write(b'%s\n' % (message))


class MyThreadingSimpleServer(ThreadingMixIn, HTTPServer):
    pass


def main():
    listen_address = os.environ.get("LISTEN_ADDRESS", "127.0.0.1")
    listen_port = os.environ.get("LISTEN_PORT", "9000")
    server = MyThreadingSimpleServer((listen_address, int(listen_port)), HTTPRequestHandler)
    print("Starting server on %s:%s..." % (listen_address, listen_port))
    server.serve_forever()


if __name__ == '__main__':
    main()
